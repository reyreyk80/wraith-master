Package: wraith-master
Version: %%VERSION%%
Section: custom
Priority: optional
Architecture: amd64
Essential: no
Installed-Size: 1024
Maintainer: serebit
Description: A Wraith Prism RGB control application for Linux, built with GTK+ and Kotlin/Native.
